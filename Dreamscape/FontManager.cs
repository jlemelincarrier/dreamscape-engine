﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamscape
{
    public class FontManager
    {
        private Dictionary<string, SpriteFont> fonts;
        private Engine engine;

        public FontManager(Engine engine)
        {
            fonts = new Dictionary<string, SpriteFont>();
            this.engine = engine;
        }

        public void Load()
        {
            foreach (string fontFile in Directory.GetFiles("Content/Fonts/"))
            {
                string key = Path.GetFileNameWithoutExtension("Fonts/" + fontFile);
                string fileToLoad = "Fonts/" + key;
                fonts.Add(key, engine.Content.Load<SpriteFont>(fileToLoad));
            }
        }

        public void RenderText(string fontType, string text, Vector2 pos, Color color)
        {
            engine.SpriteBatch.DrawString(fonts[fontType], text, pos, color);
        }
    }
}

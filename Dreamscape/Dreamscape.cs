﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

using Dreamscape.Entities;
using Dreamscape.Components;
using Dreamscape.TileEngine;
using Dreamscape.Screens;
#endregion

namespace Dreamscape
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Dreamscape : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //SpriteFont font;

        //Map starter_map;
        //Camera cam;
        //ScreenManager screenManager;
        Engine engine;

        public GraphicsDeviceManager _Graphics
        {
            get
            {
                return graphics;
            }
        }

        public SpriteBatch _SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }

        public ContentManager _Content
        {
            get
            {
                return this.Content;
            }
        }

        public Dreamscape()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
            Content.RootDirectory = "Content";

            engine = new Engine();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //Globals.Global.Initialize(this);
            engine.Initialize(this);

            //starter_map = new Map();
            //starter_map.Load("Content/Maps/starter_tilemap.xml");;
            //Entity player = Globals.Global.EntityManager.RegisterEntity("player", new Entity());
            //Entity camera = Globals.Global.EntityManager.RegisterEntity("camera1", new Camera());
            //cam = Globals.Global.EntityManager.GetEntity<Camera>("default");
            //player.Load("Content/Actors/main_character.xml");
            //cam.SetTarget(player);
            //cam.AttachToMap(starter_map);

            //This will soon be unnecessary
            //screenManager = new ScreenManager(engine);

            //MapScreen mapScreen = new MapScreen(screenManager);

            MapScreen mapScreen = new MapScreen(engine);

            //screenManager.PushScreen(mapScreen);
            engine.ScreenManager.PushScreen(mapScreen);


            //Globals.Global.ScreenManager = screenManager;
            mapScreen.Load("Content/Maps/starter_tilemap.xml");

            
            //font = Content.Load<SpriteFont>("Fonts/MyFont");
            //Globals.Global.CurrentMap = starter_map;
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //Globals.Global.EntityManager.Update(gameTime);
            ////starter_map.Update(gameTime);
            //Globals.Global.CurrentMap.Update(gameTime);
            engine.ScreenManager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, SamplerState.LinearClamp, DepthStencilState.Default, RasterizerState.CullNone, null, engine.ScreenManager.CurrentScreen.Camera.Transformation);
            //starter_map.Render();
            //Globals.Global.CurrentMap.Render();
            //Globals.Global.EntityManager.Render();
            engine.ScreenManager.Render();
            //spriteBatch.DrawString(font, "Test String", new Vector2(10, 10), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}

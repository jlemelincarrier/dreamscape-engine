﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Dreamscape.TileEngine.Events;

namespace Dreamscape.Input
{
    public abstract class IAction
    {
        public abstract void Exec(BaseEventArgs args);
    }
}

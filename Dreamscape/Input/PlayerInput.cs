﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Dreamscape.Entities;
using Dreamscape.Components;
using Dreamscape.Screens;

namespace Dreamscape.Input
{
    public class PlayerInput : BaseInput
    {

        private Engine engine;

        public PlayerInput(Engine engine)
        {
            //this.AddAction(Keys.E, new UseAction());
            //this.AddAction(Keys.Enter, new StartAction());
            //this.AddAction(Buttons.A, new UseAction());
            this.engine = engine;
        }

        public PlayerInput(string filename)
        {
            //Load XML file to load in the actions
        }

        public override void Update(GameTime gameTime)
        {
            // Updates the current states of the keyboard/gamepad
            base.Update(gameTime);

            PositionComponent pos = engine.EntityManager.GetEntity("player").GetComponent<PositionComponent>();
            AnimationComponent animation = engine.EntityManager.GetEntity("player").GetComponent<AnimationComponent>();

            if (this.KeyDown(Keys.Down) || this.DPadDown() == Direction.South)
            {
                pos.Direction = Direction.South;
                pos.Move(1.0f, new Vector2(0, 1));
                animation.ChangeAnimation("down");
            }

            if (this.KeyDown(Keys.Up) || this.DPadDown() == Direction.North)
            {
                pos.Direction = Direction.North;
                pos.Move(1.0f, new Vector2(0, -1));
                animation.ChangeAnimation("up");
            }

            if (this.KeyDown(Keys.Left) || this.DPadDown() == Direction.West)
            {
                pos.Direction = Direction.West;
                pos.Move(1.0f, new Vector2(-1, 0));
                animation.ChangeAnimation("left");
            }

            if (this.KeyDown(Keys.Right) || this.DPadDown() == Direction.East)
            {
                pos.Direction = Direction.East;
                pos.Move(1.0f, new Vector2(1, 0));
                animation.ChangeAnimation("right");
            }

            if (this.KeyReleased(Keys.Enter))
            {
                //this.GetAction(Keys.E).Exec();
                //this.GetAction(Buttons.A).Exec();
                //engine.ScreenManager.PushScreen(new PauseScreen(engine.ScreenManager));
                List<NPC> npcList = engine.EntityManager.GetEntitiesOfType<NPC>();
                Entity player = engine.EntityManager.GetEntity("player");

                foreach (NPC npc in npcList)
                {
                    if (npc.InCollisionRadius(player))
                    {
                        engine.ScreenManager.PushScreen(new DialogScreen(engine.ScreenManager, "Content/Dialog/test_dialog.xml"));
                    }
                }
                
                //engine.ScreenManager.InputManager.Push(new DialogInput((DialogScreen)engine.ScreenManager.CurrentScreen));
            }


            if (this.KeyReleased(Keys.E) || this.ButtonReleased(Buttons.A))
            {
                //this.GetAction(Keys.E).Exec();
                //this.GetAction(Buttons.A).Exec();
            }
        }
    }
}

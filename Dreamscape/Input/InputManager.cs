﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Dreamscape.Input
{
    public class InputManager
    {
        private List<BaseInput> inputList;
        int inputIndex;

        public BaseInput CurrentInput
        {
            get
            {
                return inputList[inputIndex];
            }
        }

        public InputManager()
        {
            inputIndex = -1;
            inputList = new List<BaseInput>();
        }

        public void Push(BaseInput input)
        {
            inputIndex++;
            inputList.Add(input);
        }

        public void Pop()
        {
            inputList.RemoveAt(inputIndex);
            inputIndex--;

            if (inputIndex < 0)
            {
                inputIndex = -1;
            }
        }

        public void Update(GameTime gameTime)
        {
            if (inputList.Count != 0)
            {
                inputList[inputIndex].Update(gameTime);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Dreamscape.Components;

namespace Dreamscape.Input
{
    public class BaseInput
    {
        private Dictionary<Keys, IAction> keyMap;
        private Dictionary<Buttons, IAction> buttonMap;
        KeyboardState prevKeyboardState, keyboardState;
        GamePadState prevGamepadState, gamepadState;

        public BaseInput()
        {
            keyMap = new Dictionary<Keys, IAction>();
            buttonMap = new Dictionary<Buttons, IAction>();
        }

        public void AddAction(Keys key, IAction action)
        {
            keyMap[key] = action;
        }

        public void AddAction(Buttons button, IAction action){
            buttonMap[button] = action;
        }

        public IAction GetAction(Keys key)
        {
            return keyMap[key];
        }

        public IAction GetAction(Buttons button)
        {
            return buttonMap[button];
        }

        public virtual void Update(GameTime gameTime){
            prevKeyboardState = keyboardState;
            prevGamepadState = gamepadState;

            keyboardState = Keyboard.GetState();
            gamepadState = GamePad.GetState(PlayerIndex.One);
        }
        

        //Keyboard Buttons Presses

        public virtual bool KeyDown(Keys key)
        {
            return keyboardState.IsKeyDown(key);
        }

        public virtual bool KeyUp(Keys key)
        {
            return keyboardState.IsKeyUp(key);
        }

        public virtual bool KeyPressed(Keys key)
        {
            return (keyboardState.IsKeyDown(key) && prevKeyboardState.IsKeyUp(key));
        }
        
        public virtual bool KeyReleased(Keys key)
        {
            return (keyboardState.IsKeyUp(key) && prevKeyboardState.IsKeyDown(key));
        }


        //Gamepad Button Presses

        public virtual Direction DPadDown()
        {
            if (gamepadState.DPad.Down == ButtonState.Pressed)
            {
                return Direction.South;
            }

            if (gamepadState.DPad.Up == ButtonState.Pressed)
            {
                return Direction.North;
            }

            if (gamepadState.DPad.Left == ButtonState.Pressed)
            {
                return Direction.West;
            }

            if (gamepadState.DPad.Right == ButtonState.Pressed)
            {
                return Direction.East;
            }

            return Direction.Idle;
        }

        public virtual bool ButtonPressed(Buttons button)
        {
            return gamepadState.IsButtonDown(button);
        }

        public virtual bool ButtonReleased(Buttons button)
        {
            return (gamepadState.IsButtonUp(button) && prevGamepadState.IsButtonDown(button));
        }
    }
}

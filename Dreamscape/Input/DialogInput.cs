﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

using Dreamscape.Screens;

namespace Dreamscape.Input
{
    public class DialogInput : BaseInput
    {
        private DialogScreen dialogScreen;
        public DialogInput(DialogScreen screen)
            : base()
        {
            dialogScreen = screen;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (this.KeyReleased(Keys.Enter))
            {
                //this.GetAction(Keys.E).Exec();
                //this.GetAction(Buttons.A).Exec();
                
                dialogScreen.NextDialog();
            }
        }
    }
}

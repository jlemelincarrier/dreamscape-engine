﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Dreamscape.Input
{
    public class PauseScreenInput : BaseInput
    {
        private Engine engine;

        public PauseScreenInput(Engine engine)
            : base()
        {
            this.engine = engine;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (this.KeyReleased(Keys.Enter))
            {
                //this.GetAction(Keys.E).Exec();
                //this.GetAction(Buttons.A).Exec();
                engine.ScreenManager.PopScreen();
                engine.ScreenManager.CurrentScreen.Updateable = true;
            }
        }
    }
}

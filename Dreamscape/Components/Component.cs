﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Dreamscape.Entities;

namespace Dreamscape.Components
{
    public class Component
    {
        private string componentId;
        private Entity owner;
        bool isUpdateable, isRenderable;

        public bool IsUpdateable
        {
            set
            {
                isUpdateable = value;
            }

            get
            {
                return isUpdateable;
            }
        }

        public bool IsRenderable
        {
            set
            {
                isRenderable = value;
            }

            get
            {
                return isRenderable;
            }
        }

        public string ComponentId
        {
            get
            {
                return componentId;
            }
        }

        public Entity Owner
        {
            get
            {
                return owner;
            }
        }

        public Component(string id, Entity owner)
        {
            this.componentId = id;
            this.owner = owner;
        }

        public virtual void Update(GameTime gameTime){

        }

        public virtual void Render()
        {

        }
    }
}

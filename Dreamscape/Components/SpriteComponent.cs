﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Dreamscape.Entities;

namespace Dreamscape.Components
{
    public class SpriteComponent : Component
    {
        Texture2D sprite;
        int width, height;

        public SpriteComponent(Entity owner, string filename, int width, int height)
            : base("Sprite", owner)
        {
            this.IsRenderable = true;
            sprite = Globals.Global.Content.Load<Texture2D>(filename);
            this.width = width;
            this.height = height;
        }

        public override void Render()
        {
            Globals.Global.SpriteBatch.Draw(sprite, new Rectangle((int)Owner.GetComponent<PositionComponent>().Position.X,
                                                                        (int)Owner.GetComponent<PositionComponent>().Position.Y,
                                                                        width,
                                                                        height),
                                                                    Color.White);
            base.Render();
        }
    }
}

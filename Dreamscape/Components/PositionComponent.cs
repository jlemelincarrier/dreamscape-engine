﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Dreamscape.Entities;

namespace Dreamscape.Components
{
    public enum Direction
    {
        Idle,
        North,
        South,
        East,
        West
    };
    public class PositionComponent : Component
    {
        
        public Direction Direction
        {
            set
            {
                direction = value;
            }
            get
            {
                return direction;
            }
        }

        private Direction direction;
        private Vector2 position;
        private float velocity;

        public float Velocity
        {
            get
            {
                return velocity;
            }
        }
        
        public Vector2 Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        public PositionComponent(Entity owner)
            : base("Position", owner)
        {
            position = Vector2.Zero;
        }

        public PositionComponent(Entity owner, Vector2 pos) 
            : base("Position", owner)
        {
            position = pos;
        }

        public void Load(XmlNode node)
        {
            XmlNode pos = node.SelectSingleNode("Position");
            int x = Convert.ToInt16(pos.Attributes.GetNamedItem("x").Value);
            int y = Convert.ToInt16(pos.Attributes.GetNamedItem("y").Value);

            position = new Vector2((float)x, (float)y);
        }

        public void Move(float vel, Vector2 dir)
        {
            velocity = vel;
            Vector2 distance = Vector2.Multiply(dir, vel);
            position += distance;

            //position.X = MathHelper.Clamp(position.X, 0, Globals.Global.CurrentMap.MapWidth * Globals.Global.CurrentMap.TileWidth);
            //position.Y = MathHelper.Clamp(position.Y, 0, Globals.Global.CurrentMap.MapHeight * Globals.Global.CurrentMap.TileHeight);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Dreamscape.Input;
using Dreamscape.Entities;
using Microsoft.Xna.Framework;

namespace Dreamscape.Components
{
    public class InputComponent : Component
    {
        BaseInput input;

        public InputComponent(Entity owner)
            : base("InputComponent", owner)
        {
            this.IsUpdateable = true;
        }

        public void Load(XmlNode node)
        {
            XmlNode input = node.SelectSingleNode("Input");
            string inputType = input.Attributes.GetNamedItem("inputType").Value;

            if(inputType == "PlayerInput")
            {
                this.input = new PlayerInput(Owner.EntityManager.Engine);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            input.Update(gameTime);
        }
    }
}

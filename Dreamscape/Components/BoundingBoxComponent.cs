﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Dreamscape.Entities;

namespace Dreamscape.Components
{
    public class BoundingBoxComponent : Component
    {
        private Rectangle boundingBox;
        private Texture2D collisionTex;

        public Rectangle AABB
        {
            //get
            //{
            //    PositionComponent pos = Owner.GetComponent<PositionComponent>();

            //    if (boundingBox.X + boundingBox.Width + pos.Position.X > Globals.Global.CurrentMap.MapWidth * Globals.Global.CurrentMap.TileWidth){
            //        pos.Position = new Vector2(Globals.Global.CurrentMap.MapWidth * Globals.Global.CurrentMap.TileWidth - boundingBox.Width - boundingBox.X, pos.Position.Y);
            //    }else if (pos.Position.X < 0)
            //    {
            //        pos.Position = new Vector2(0, pos.Position.Y);
            //    }

            //    if (boundingBox.Y + boundingBox.Height + pos.Position.Y > Globals.Global.CurrentMap.MapHeight * Globals.Global.CurrentMap.TileHeight)
            //    {
            //        pos.Position = new Vector2(pos.Position.X, Globals.Global.CurrentMap.MapHeight * Globals.Global.CurrentMap.TileHeight - boundingBox.Height - boundingBox.Y);
            //    }else if(pos.Position.Y < 0){
            //        pos.Position = new Vector2(pos.Position.X, 0);
            //    }

            //    return new Rectangle((int)(boundingBox.X + pos.Position.X), (int)(boundingBox.Y + pos.Position.Y), boundingBox.Width, boundingBox.Height);
            //}
            get
            {
                PositionComponent pos = Owner.GetComponent<PositionComponent>();

                if (boundingBox.X + boundingBox.Width + pos.Position.X > Owner.EntityManager.Engine.CurrentMap.MapWidth * Owner.EntityManager.Engine.CurrentMap.TileWidth)
                {
                    pos.Position = new Vector2(Owner.EntityManager.Engine.CurrentMap.MapWidth * Owner.EntityManager.Engine.CurrentMap.TileWidth - boundingBox.Width - boundingBox.X, pos.Position.Y);
                }
                else if (pos.Position.X < 0)
                {
                    pos.Position = new Vector2(0, pos.Position.Y);
                }

                if (boundingBox.Y + boundingBox.Height + pos.Position.Y > Owner.EntityManager.Engine.CurrentMap.MapHeight * Owner.EntityManager.Engine.CurrentMap.TileHeight)
                {
                    pos.Position = new Vector2(pos.Position.X, Owner.EntityManager.Engine.CurrentMap.MapHeight * Owner.EntityManager.Engine.CurrentMap.TileHeight - boundingBox.Height - boundingBox.Y);
                }
                else if (pos.Position.Y < 0)
                {
                    pos.Position = new Vector2(pos.Position.X, 0);
                }

                return new Rectangle((int)(boundingBox.X + pos.Position.X), (int)(boundingBox.Y + pos.Position.Y), boundingBox.Width, boundingBox.Height);
            }
        }

        public BoundingBoxComponent(Entity owner)
            : base("BoundingBoxComponent", owner)
        {
            this.IsRenderable = true;
            this.IsUpdateable = true;
        }

        public void Load(XmlNode node)
        {
            XmlNode boundingBox = node.SelectSingleNode("BoundingBox");

            int posX = Convert.ToInt16(boundingBox.Attributes.GetNamedItem("x").Value);
            int posY = Convert.ToInt16(boundingBox.Attributes.GetNamedItem("y").Value);
            int width = Convert.ToInt16(boundingBox.Attributes.GetNamedItem("width").Value);
            int height = Convert.ToInt16(boundingBox.Attributes.GetNamedItem("height").Value);

            string tex = boundingBox.Attributes.GetNamedItem("debugTexture").Value;
            if (tex != "null")
            {
                this.collisionTex = Owner.EntityManager.Engine.Content.Load<Texture2D>(tex);
            }

            this.boundingBox = new Rectangle(posX, posY, width, height);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Render()
        {
 	        base.Render();
            Owner.EntityManager.Engine.SpriteBatch.Draw(collisionTex,
                this.AABB,
                Color.White);
        }

        public void Collide(Rectangle entityBounds)
        {
            if(AABB.Intersects(entityBounds)){
                //Distance between the centers of the AABBs
                Point distance = new Point(AABB.Center.X - entityBounds.Center.X, AABB.Center.Y - entityBounds.Center.Y);
            
                //Sum of the halfwidths
                float width = (AABB.Width / 2) + (entityBounds.Width / 2);
                float height = (AABB.Height / 2) + (entityBounds.Height / 2);

                Vector2 depth = new Vector2(width - Math.Abs(distance.X), height - Math.Abs(distance.Y));

                //If the distance between the centers is less than the sum of halfwidths, a collision has occurred
                PositionComponent pos = Owner.GetComponent<PositionComponent>();
                if (depth.X < depth.Y)
                {
                    if (distance.X < 0)
                    {
                        pos.Move(pos.Velocity, new Vector2(-1, 0));
                    }
                    else
                    {
                        pos.Move(pos.Velocity, new Vector2(1, 0));
                    }
                }
                else
                {
                    if (distance.Y > 0)
                    {
                        pos.Move(pos.Velocity, new Vector2(0, 1));
                    }
                    else
                    {
                        pos.Move(pos.Velocity, new Vector2(0, -1));
                    }
                }
            }
        }
    }
}

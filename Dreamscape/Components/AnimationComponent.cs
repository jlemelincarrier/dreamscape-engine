﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Dreamscape.Entities;

namespace Dreamscape.Components
{
    public class AnimationComponent : Component
    {
        private Dictionary<string, Animation> animationList;
        private Texture2D spritesheet;
        private string currentAnimation;

        public void ChangeAnimation(string name)
        {
            currentAnimation = name;
        }

        public Animation CurrentAnimation
        {
            get
            {
                return animationList[currentAnimation];
            }
        }

        public AnimationComponent(Entity owner)
            : base("Animation", owner)
        {
            animationList = new Dictionary<string, Animation>();
            IsRenderable = true;
            IsUpdateable = true;
        }

        public void Load(XmlNode node)
        {
            XmlNodeList animations = node.SelectNodes("Animation");
            foreach (XmlNode animationNode in animations)
            {
                string animationName = animationNode.Attributes.GetNamedItem("animationName").Value;
                currentAnimation = animationName;

                int frameWidth = Convert.ToInt16(animationNode.Attributes.GetNamedItem("frameWidth").Value);
                int frameHeight = Convert.ToInt16(animationNode.Attributes.GetNamedItem("frameHeight").Value);
                string spritesheetName = animationNode.Attributes.GetNamedItem("spritesheet").Value;
                float timeBetweenFrames = Convert.ToSingle(animationNode.Attributes.GetNamedItem("timeBetweenFrames").Value);

                spritesheet = Owner.EntityManager.Engine.Content.Load<Texture2D>(spritesheetName);

                Animation animation = new Animation(frameWidth, frameHeight, spritesheet, timeBetweenFrames);

                XmlNodeList frames = animationNode.SelectNodes("Frame");

                foreach (XmlNode frameNode in frames)
                {
                    int texPosX = Convert.ToInt16(frameNode.Attributes.GetNamedItem("texPosX").Value);
                    int texPosY = Convert.ToInt16(frameNode.Attributes.GetNamedItem("texPosY").Value);
                    animation.AddFrame(new Rectangle(texPosX * frameWidth, texPosY * frameHeight, frameWidth, frameHeight));
                }

                animationList.Add(animationName, animation);
            }
        }

        public override void Update(GameTime gameTime)
        {
            animationList[currentAnimation].Update(gameTime);
            base.Update(gameTime);
        }

        public override void Render()
        {
            Owner.EntityManager.Engine.SpriteBatch.Draw(spritesheet, Owner.GetComponent<PositionComponent>().Position, this.CurrentAnimation.CurrentFrame, Color.White); 
            base.Render();
        }
    }
}

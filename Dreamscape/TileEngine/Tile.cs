﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Dreamscape.TileEngine
{
    public enum TileType
    {
        None,
        Teleport,
        Event
    };

    public class Tile
    {
        private Vector2 position;
        private Vector2 texturePos;
        private int objectId;
        private int width, height;
        private Rectangle boundingBox;
        private TileType type;
        
        public TileType Type
        {
            set
            {
                type = value;
            }
            get
            {
                return type;
            }
        }
        //Tiles can contain triggers/events

        public Vector2 Position
        {
            set
            {
                position = value;
            }
            get
            {
                return position;
            }
        }

        public Vector2 TexturePosition
        {
            set
            {
                texturePos = value;
            }
            get
            {
                return texturePos;
            }
        }

        public int ObjectId
        {
            get
            {
                return objectId;
            }
        }

        public int TileWidth
        {
            get
            {
                return width;
            }
        }

        public int TileHeight
        {
            get
            {
                return height;
            }
        }

        public Rectangle BoundingBox
        {
            get
            {
                return boundingBox;
            }
        }

        public bool Blocked
        {
            get;
            set;
        }

        public Tile()
        {

        }

        public Tile(int objectId, Vector2 position, Vector2 texturePos, int width, int height)
        {
            this.position = position;
            this.texturePos = texturePos;
            this.objectId = objectId;
            this.width = width;
            this.height = height;

            this.boundingBox = new Rectangle((int)position.X * width, (int)position.Y * height, width, height);
        }
    }
}

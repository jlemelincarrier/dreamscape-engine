﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Dreamscape.Entities;
using Dreamscape.Components;
using Dreamscape.Screens;
using Dreamscape.Screens.Transitions;
using Dreamscape.Input;

namespace Dreamscape.TileEngine
{
    public class Map
    {
        Dictionary<string, Layer> layers;
        XmlDocument mapDocument;
        private int width, height;
        private MapScreen mapScreen;
        private FadeTransition transition;
        private Dictionary<string, NPC> npcList;

        public FadeTransition Transition
        {
            get
            {
                return transition;
            }
        }

        public int MapWidth
        {
            set
            {
                width = value;
            }
            get
            {
                return width;
            }
        }

        public int MapHeight
        {
            set
            {
                height = value;
            }
            get
            {
                return height;
            }
        }

        public int TileHeight
        {
            get
            {
                return layers["MapLayer"].TileHeight;
            }
        }

        public int TileWidth
        {
            get
            {
                return layers["MapLayer"].TileWidth;
            }
        }

        public Map(MapScreen screen)
        {
            layers = new Dictionary<string, Layer>();
            mapDocument = new XmlDocument();
            mapScreen = screen;
            transition = new FadeTransition(mapScreen.Engine, 1.0f);
            npcList = new Dictionary<string, NPC>();
            mapScreen.Engine.ScreenManager.InputManager.Push(new PlayerInput(mapScreen.Engine));
        }

        public Map(int width, int height)
        {
            layers = new Dictionary<string, Layer>();
            mapDocument = new XmlDocument();
            this.width = width;
            this.height = height;
        }

        public void Load(string filename){
            mapDocument.Load(filename);

            XmlNode mapNode = mapDocument.SelectSingleNode("/TileMap");
            this.width = Convert.ToInt16(mapNode.Attributes.GetNamedItem("width").Value);
            this.height = Convert.ToInt16(mapNode.Attributes.GetNamedItem("height").Value);

            //Load the Base Map Layer
            Layer mapLayer = new Layer(mapScreen.Engine);
            mapLayer.LayerWidth = this.width;
            mapLayer.LayerHeight = this.height;
            mapLayer.Load(mapDocument, "/TileMap/MapLayer");
            layers.Add(mapLayer.LayerId, mapLayer);

            Layer collisionLayer = new Layer(mapScreen.Engine);
            collisionLayer.LayerWidth = this.width;
            collisionLayer.LayerHeight = this.height;
            collisionLayer.Load(mapDocument, "TileMap/CollisionLayer");
            layers.Add(collisionLayer.LayerId, collisionLayer);

            Layer objectLayer = new Layer(mapScreen.Engine);
            objectLayer.LayerWidth = this.width;
            objectLayer.LayerHeight = this.height;
            objectLayer.Load(mapDocument, "TileMap/ObjectLayer");
            layers.Add(objectLayer.LayerId, objectLayer);

            //Load NPCS
            XmlNode npcLayer = mapDocument.SelectSingleNode("TileMap/NPCLayer");

            if (npcLayer != null)
            {
                XmlNodeList npcNodeList = npcLayer.SelectNodes("NPC");

                foreach (XmlNode npcNode in npcNodeList)
                {
                    string npcId = npcNode.Attributes.GetNamedItem("id").Value;
                    string npcFile = npcNode.Attributes.GetNamedItem("file").Value;

                    Entity npcEnt = mapScreen.Engine.EntityManager.RegisterEntity(npcId, new NPC());
                    NPC npc = mapScreen.Engine.EntityManager.GetEntity<NPC>("npc1");
                    npc.Load(npcFile);

                    npcList.Add(npc.ObjectId, npc);
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            List<Entity> entityList = mapScreen.Engine.EntityManager.GetEntitiesWithComponent<BoundingBoxComponent>();

            foreach (Entity entity in entityList)
            {
                Tile temp;

                //Make sure to clamp the entity positions in order to not get index out of bounds
                BoundingBoxComponent bb = entity.GetComponent<BoundingBoxComponent>();

                int tileX = (int)(bb.AABB.Center.X / layers["CollisionLayer"].TileWidth);
                int tileY = (int)(bb.AABB.Center.Y / layers["CollisionLayer"].TileHeight);
                temp = layers["CollisionLayer"].GetTile(tileX, tileY);
                                
                bb.Collide(temp.BoundingBox);

                if (entity.ObjectId == "player")
                {
                    int x = (int)(bb.AABB.Center.X / layers["ObjectLayer"].TileWidth);
                    int y = (int)(bb.AABB.Center.Y / layers["ObjectLayer"].TileHeight);

                    temp = layers["ObjectLayer"].GetTile(tileX, tileY);
                    
                    if (temp.Type == TileType.Teleport)
                    {
                        Teleport teleport = (Teleport)temp;

                        teleport.TeleportArgs.EntityToMove = entity;
                        teleport.Trigger += new Teleport.TeleportEventHandler(TeleportPlayer);

                        teleport.OnTrigger(teleport.TeleportArgs);
                    }

                    //Add Collision radius detection for interactions
                    //if(){

                    //}
                }
            }


        }

        public void Render()
        {
            foreach (KeyValuePair<string, Layer> layer in layers)
            {
                if (layer.Value.IsRenderable)
                {
                    layer.Value.Render();
                }
            }
        }

        public void TeleportPlayer(TeleportEventArgs args)
        {
            // Use the arguments to load the new map and unload the current one.

            this.mapScreen.Engine.ScreenManager.InputManager.Pop();
            
            this.Transition.Transitioning = true;
            this.Transition.FadingOut = true;
            this.Transition.FadingIn = false;
            this.Transition.TransitionAlpha = 0.0f;
            this.Transition.TransitionComplete = false;

            this.Transition.OnFadeIn(args);
            this.Transition.FadeInCallback += new Screens.Transitions.FadeTransition.FadeIn(TransitionFadeIn);
        }

        void TransitionFadeIn(TeleportEventArgs args)
        {
            foreach (KeyValuePair<string, NPC> npc in npcList)
            {
                mapScreen.Engine.EntityManager.UnregisterEntity(npc.Value.ObjectId);
            }
            npcList.Clear();

            Map nextMap = new Map(mapScreen);
            nextMap.Load(args.NextMap);

            nextMap.Transition.Transitioning = true;
            nextMap.Transition.FadingIn = true;
            nextMap.Transition.FadingOut = false;
            nextMap.Transition.TransitionAlpha = this.Transition.TransitionAlpha;

            mapScreen.CurrentMap = nextMap;
            PositionComponent pos = args.EntityToMove.GetComponent<PositionComponent>();
            pos.Position = args.SpawnPosition;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Dreamscape.Entities;

namespace Dreamscape.TileEngine
{
    public class TeleportEventArgs : EventArgs
    {
        public Entity EntityToMove
        {
            get;
            set;
        }

        public Vector2 SpawnPosition
        {
            get;
            set;
        }

        //public Map NextMap
        //{
        //    get;
        //    set;
        //}

        public string NextMap
        {
            get;
            set;
        }
    }
}

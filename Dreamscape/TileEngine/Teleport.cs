﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Dreamscape.TileEngine
{
    public class Teleport : Tile
    {
        public delegate void TeleportEventHandler(TeleportEventArgs teleportArgs);
        public event TeleportEventHandler Trigger;
        private TeleportEventArgs args;

        public Teleport(int objectId, Vector2 position, Vector2 texturePos, int width, int height)
            : base(objectId, position, texturePos, width, height)
        {
            args = new TeleportEventArgs();
        }

        public TeleportEventArgs TeleportArgs
        {
            get
            {
                return args;
            }
        }

        public void OnTrigger(TeleportEventArgs teleportArgs)
        {
            if (Trigger != null)
            {
                Trigger(teleportArgs);
            }
        }
    }
}

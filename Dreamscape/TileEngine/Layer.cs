﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Dreamscape.Entities;

namespace Dreamscape.TileEngine
{
    class Layer
    {
        List<List<Tile>> tileList;
        private string layerId;
        private Texture2D layerTexture;
        private int width, height, tileWidth, tileHeight;
        private bool isRenderable;
        Engine engine;

        public bool IsRenderable
        {
            get
            {
                return isRenderable;
            }
            set
            {
                isRenderable = value;
            }
        }

        public string LayerId
        {
            set
            {
                layerId = value;
            }

            get
            {
                return layerId;
            }
        }

        public int LayerWidth
        {
            set
            {
                width = value;
            }
            get
            {
                return width;
            }
        }

        public int LayerHeight
        {
            set
            {
                height = value;
            }
            get
            {
                return height;
            }
        }

        public int TileWidth
        {
            get
            {
                return tileWidth;
            }
        }

        public int TileHeight
        {
            get
            {
                return tileHeight;
            }
        }

        public Layer(Engine engine)
        {
            this.engine = engine;
        }

        public void Load(XmlDocument doc, string layerToLoad)
        {
            tileList = new List<List<Tile>>();

            for(int y = 0; y < height; y++){
                List<Tile> tempList = new List<Tile>(width);
                for (int x = 0; x < width; x++)
                {
                    tempList.Add(new Tile());
                }
                tileList.Insert(y, tempList);
            }

            XmlNode layerNode = doc.SelectSingleNode(layerToLoad);

            layerId = layerNode.Attributes.GetNamedItem("layerId").Value;
            layerTexture = engine.Content.Load<Texture2D>(layerNode.Attributes.GetNamedItem("layerTexture").Value);
            this.isRenderable = Convert.ToBoolean(layerNode.Attributes.GetNamedItem("isRenderable").Value);

            XmlNode tileNodeListNode = layerNode.SelectSingleNode("TileList");
            tileWidth = Convert.ToInt16(tileNodeListNode.Attributes.GetNamedItem("tileWidth").Value);
            tileHeight = Convert.ToInt16(tileNodeListNode.Attributes.GetNamedItem("tileHeight").Value);

            XmlNodeList tileNodeList = tileNodeListNode.SelectNodes("Tile");           

            foreach (XmlNode tileNode in tileNodeList)
            {
                if (tileNode == null)
                {
                    break;
                }
                float posX = Convert.ToSingle(tileNode.Attributes.GetNamedItem("x").Value);
                float posY = Convert.ToSingle(tileNode.Attributes.GetNamedItem("y").Value);
                Vector2 pos = new Vector2(posX, posY);
                Vector2 texPos = new Vector2(Convert.ToSingle(tileNode.Attributes.GetNamedItem("texPosX").Value), Convert.ToSingle(tileNode.Attributes.GetNamedItem("texPosY").Value));
                int objectId = Convert.ToInt16(tileNode.Attributes.GetNamedItem("objectId").Value);


                string tiletype = tileNode.Attributes.GetNamedItem("type").Value;
                
                if (tiletype != "none")
                {
                    if (tiletype == "Teleport")
                    {
                        Teleport temp = new Teleport(objectId, pos, texPos, tileWidth, tileHeight);
                        temp.Type = TileType.Teleport;

                        XmlNode teleportNode = tileNode.SelectSingleNode("Teleport");

                        temp.TeleportArgs.SpawnPosition = new Vector2(Convert.ToInt16(teleportNode.Attributes.GetNamedItem("spawnPosX").Value), Convert.ToInt16(teleportNode.Attributes.GetNamedItem("spawnPosY").Value));
                        //Map nextMap = new Map(mapScreen);
                        //nextMap.Load(teleportNode.Attributes.GetNamedItem("nextMap").Value);
                        //temp.TeleportArgs.NextMap = nextMap;

                        temp.TeleportArgs.NextMap = teleportNode.Attributes.GetNamedItem("nextMap").Value;

                        tileList[(int)pos.Y][(int)pos.X] = temp;
                    }
                }
                else
                {
                    Tile temp = new Tile(objectId, pos, texPos, tileWidth, tileHeight);
                    tileList[(int)pos.Y][(int)pos.X] = temp;
                }

                //tileList[(int)pos.Y][(int)pos.X] = temp;
            }
        }

        public void Unload()
        {
            tileList.Clear();
        }

        public Tile GetTile(int x, int y)
        {
            return tileList[y][x];
        }

        public void Render()
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    engine.SpriteBatch.Draw(layerTexture,
                                                    tileList[y][x].BoundingBox,
                                                    new Rectangle((int)tileList[y][x].TexturePosition.X * tileList[y][x].TileWidth, (int)tileList[y][x].TexturePosition.Y * tileList[y][x].TileHeight, tileList[y][x].TileWidth, tileList[y][x].TileHeight),
                                                    Color.White);
                }
            }
        }
    }
}

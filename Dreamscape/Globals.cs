﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Dreamscape.Entities;
using Dreamscape.TileEngine;

namespace Dreamscape
{
    public class Globals
    {
        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;
        ContentManager content;
        EntityManager entityManager;
        FontManager fontManager;
        ScreenManager screenManager;
        Map currentMap;

        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }

        public SpriteBatch SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }

        public ContentManager Content
        {
            get
            {
                return content;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                return entityManager;
            }
        }

        public FontManager FontManager
        {
            get
            {
                return fontManager;
            }
        }

        public ScreenManager ScreenManager
        {
            set
            {
                screenManager = value;
            }
            get
            {
                return screenManager;
            }
        }

        public Map CurrentMap
        {
            set
            {
                currentMap = value;
            }
            get
            {
                return currentMap;
            }
        }

        public Globals()
        {
            //entityManager = new EntityManager();
            //fontManager = new FontManager();
        }

        public void Initialize(Dreamscape dreamscape)
        {
            this.spriteBatch = dreamscape._SpriteBatch;
            this.graphics = dreamscape._Graphics;
            this.content = dreamscape._Content;
            fontManager.Load();
        }

        private static Globals _instance;
        public static Globals Global
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Globals();
                }

                return _instance;
            }
        }
    }
}

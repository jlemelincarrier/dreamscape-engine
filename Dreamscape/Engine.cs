﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Dreamscape.Entities;
using Dreamscape.TileEngine;

namespace Dreamscape
{
    public class Engine
    {
        SpriteBatch spriteBatch;
        GraphicsDeviceManager graphics;
        ContentManager content;
        EntityManager entityManager;
        FontManager fontManager;
        ScreenManager screenManager;

        public GraphicsDeviceManager Graphics
        {
            get
            {
                return graphics;
            }
        }

        public SpriteBatch SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }

        public ContentManager Content
        {
            get
            {
                return content;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                return entityManager;
            }
        }

        public FontManager FontManager
        {
            get
            {
                return fontManager;
            }
        }

        public ScreenManager ScreenManager
        {
            get
            {
                return screenManager;
            }
        }

        public Map CurrentMap
        {
            get;
            set;
        }

        public Engine()
        {
            entityManager = new EntityManager(this);
            fontManager = new FontManager(this);
            screenManager = new ScreenManager(this);
        }

        public void Initialize(Dreamscape dreamscape)
        {
            this.spriteBatch = dreamscape._SpriteBatch;
            this.graphics = dreamscape._Graphics;
            this.content = dreamscape._Content;
            fontManager.Load();
        }
    }
}

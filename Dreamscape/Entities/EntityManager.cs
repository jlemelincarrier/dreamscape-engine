﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dreamscape.Components;
using Microsoft.Xna.Framework;

namespace Dreamscape.Entities
{
    public class EntityManager
    {
        Dictionary<string, Entity> entityList;
        private Engine engine;

        public Engine Engine
        {
            get
            {
                return engine;
            }
        }

        public EntityManager(Engine engine)
        {
            entityList = new Dictionary<string, Entity>();
            this.engine = engine;
        }

        public Entity GetEntity(string entityId)
        {
            return entityList[entityId];
        }

        public T GetEntity<T>(string id) where T : Entity{
            foreach (KeyValuePair<string, Entity> entity in entityList)
            {
                if (entity.Value is T && entity.Value.ObjectId == id)
                {
                    return (T)entity.Value;
                }
            }
            return null;
        }

        public Entity RegisterEntity(string entityId, Entity e)
        {
            e.ObjectId = entityId;
            e.EntityManager = this;
            entityList.Add(entityId, e);

            return e;
        }

        public void UnregisterEntity(string entityId)
        {
            entityList.Remove(entityId);
        }

        public List<Entity> GetEntitiesWithComponent<T>() where T : Component
        {
            List<Entity> tempList = new List<Entity>();
            foreach (KeyValuePair<string, Entity> entity in entityList)
            {
                if (entity.Value.GetComponent<T>() != null)
                {
                    tempList.Add(entity.Value);
                }
            }

            return tempList;
        }

        public List<T> GetEntitiesOfType<T>() where T : Entity
        {
            List<T> tempList = new List<T>();
            foreach (KeyValuePair<string, Entity> entity in entityList)
            {
                if (entity.Value is T)
                {
                    tempList.Add((T)entity.Value);
                }
            }
            return tempList;
        }

        public void Update(GameTime gameTime)
        {
            foreach (KeyValuePair<string, Entity> entity in entityList)
            {
                entity.Value.Update(gameTime);
            }
        }

        public void Render()
        {
            foreach (KeyValuePair<string, Entity> entity in entityList)
            {
                entity.Value.Render();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Dreamscape.TileEngine;
using Dreamscape.Entities;
using Dreamscape.Components;

namespace Dreamscape.Entities
{
    /// <summary>
    /// \class Camera
    /// \brief Camera class which inherits from Entity
    /// 
    /// A camera is considered an entity. It contains components as well as references to a target Entity (for a following mode), and to a
    /// map in order to constrain it map boundaries.
    /// </summary>
    public class Camera : Entity
    {
        private Matrix transformation;          ///< Transformation Matrix to create the scrolling translations
        private Viewport viewport;              ///< Reference member to a viewport to get the bounds of the client window
        private Entity target;                  ///< Target entity for the camera to follow. Allowed: target = null;
        private PositionComponent targetPos;    ///< Reference to position component of target, if a target is set.
        private PositionComponent position;     ///< Reference to own position component
        private Map map;                        ///< Reference to a map that the camera is attached to.
        private Engine engine;
        
        ///
        /// A public property to return the transformation applied to the camera
        /// for uses such as scrolling.
        ///


        public Matrix Transformation
        {
            get
            {
                transformation = Matrix.CreateTranslation(new Vector3(-this.GetComponent<PositionComponent>().Position, 0));
                return transformation;
            }
        }

        /// <summary>
        /// A public property to return the camera viewport based on it's
        /// current world position and client size.
        /// </summary>

        public Rectangle CameraView
        {
            get
            {
                return new Rectangle((int)transformation.Translation.X, (int)transformation.Translation.Y, viewport.Width, viewport.Height);
            }
        }

        
        /// Camera Class Constructor
        /// 
        /// Initializes a position component as well as gets a reference to the game viewport
        ///

        public Camera(Engine engine)
            : base()
        {
            this.AddComponent(new PositionComponent(this));
            this.engine = engine;
            viewport = engine.Graphics.GraphicsDevice.Viewport;
            position = this.GetComponent<PositionComponent>();
        }

        /// <summary>
        /// A function to attach the camera to an entity to follow it
        /// @param e An Entity to attach the camera to.
        /// </summary>

        public void SetTarget(Entity e)
        {
            target = e;
            targetPos = target.GetComponent<PositionComponent>();
        }

        /// <summary>
        /// A function to attach the camera to a map
        /// @param map A map to attach the camera to.
        /// </summary>

        public void AttachToMap(Map map)
        {
            this.map = map;
        }

        /// <summary>
        /// \brief Camera update function
        /// 
        /// Update the camera position based on the constraints of the map width and height.
        /// </summary>

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            float camPosX = MathHelper.Clamp(targetPos.Position.X - (viewport.Width / 2), 0.0f, (map.MapWidth * map.TileWidth) - viewport.Width);
            float camPosY = MathHelper.Clamp(targetPos.Position.Y - (viewport.Height / 2), 0.0f, (map.MapHeight * map.TileHeight) - viewport.Height);
            //c.Position = new Vector2(targetPos.Position.X - (viewport.Width / 2), targetPos.Position.Y - (viewport.Height / 2));
            position.Position = new Vector2(camPosX, camPosY);
        }
    }
}

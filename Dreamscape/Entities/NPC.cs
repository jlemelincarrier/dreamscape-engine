﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Dreamscape.Components;

namespace Dreamscape.Entities
{
    public class NPC : Entity
    {
        private float radius;

        public NPC()
            : base()
        {
            radius = 30.0f;
        }

        public override void Load(string filename)
        {
            base.Load(filename);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            //Update after base so that components can update first
        }

        public override void Render()
        {
            base.Render();
        }

        public bool InCollisionRadius(Entity entity)
        {
            BoundingBoxComponent bb = entity.GetComponent<BoundingBoxComponent>();
            BoundingBoxComponent aabb = this.GetComponent<BoundingBoxComponent>();

            Vector2 pos1 = new Vector2(bb.AABB.Center.X, bb.AABB.Center.Y);
            Vector2 pos2 = new Vector2(aabb.AABB.Center.X, aabb.AABB.Center.Y);

            float distance = Vector2.Distance(pos1, pos2);

            if (Math.Abs(distance) <= radius)
            {
                return true;
            }

            return false;
        }
    }
}

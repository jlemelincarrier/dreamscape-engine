﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamscape.Entities
{
    public class Animation
    {
        private List<Rectangle> renderFrames;
        private int currentFrame;
        private int numFrames;
        private int frameWidth, frameHeight;
        Texture2D spritesheet;
        float elapsedTime, timeBetweenFrames;

        public Rectangle CurrentFrame
        {
            get
            {
                return renderFrames[currentFrame];
            }
        }

        public Animation(int frameWidth, int frameHeight, Texture2D spritesheet, float timeBetweenFrames)
        {
            renderFrames = new List<Rectangle>();
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.spritesheet = spritesheet;
            currentFrame = numFrames = 0;
            this.timeBetweenFrames = timeBetweenFrames;
        }

        public void AddFrame(Rectangle frame)
        {
            renderFrames.Add(frame);
            numFrames++;
        }

        public void Update(GameTime gameTime)
        {
            elapsedTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (elapsedTime >= timeBetweenFrames)
            {
                elapsedTime = 0.0f;
                currentFrame++;
                if (currentFrame >= numFrames)
                {
                    currentFrame = 0;
                }
            }
        }
    }
}

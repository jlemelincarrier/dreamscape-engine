﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Dreamscape.Components;
using Dreamscape.Input;

namespace Dreamscape.Entities
{
    
    public class Entity
    {
        private string objectId;
        private XmlDocument entityDoc;
        private EntityManager entityManager;
        private Dictionary<string, Component> components;

        public string ObjectId
        {
            set
            {
                objectId = value;
            }
            get
            {
                return objectId;
            }
        }

        public EntityManager EntityManager
        {
            set
            {
                entityManager = value;
            }
            get
            {
                return entityManager;
            }
        }

        public Entity()
        {
            components = new Dictionary<string, Component>();
        }

        public virtual void Load(string filename)
        {
            entityDoc = new XmlDocument();
            entityDoc.Load(filename);

            XmlNodeList components = entityDoc.SelectNodes("/Entity/Component");
            
            foreach (XmlNode component in components)
            {
                Component c = this.CreateComponentFromNode(component);
                this.AddComponent(c);                
            }
        }

        public void AddComponent(Component c)
        {
            components.Add(c.ComponentId, c);
        }

        public T GetComponent<T>() where T : Component
        {
            foreach (KeyValuePair<string, Component> component in components)
            {
                if (component.Value is T)
                {
                    return (T)component.Value;
                }
            }
            return null;
        }

        public Component CreateComponentFromNode(XmlNode node)
        {
            string name = node.Attributes.GetNamedItem("name").Value;

            if (name == "AnimationComponent")
            {
                AnimationComponent component = new AnimationComponent(this);
                component.Load(node);

                return component;
            }
            else if (name == "PositionComponent")
            {
                PositionComponent component = new PositionComponent(this);
                component.Load(node);

                return component;
            }
            else if (name == "InputComponent")
            {
                InputComponent component = new InputComponent(this);
                component.Load(node);

                return component;
            }
            else if (name == "BoundingBoxComponent")
            {
                BoundingBoxComponent component = new BoundingBoxComponent(this);
                component.Load(node);

                return component;
            }

            return null;
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (KeyValuePair<string, Component> component in components)
            {
                if (component.Value.IsUpdateable)
                {
                    component.Value.Update(gameTime);
                }
            }
        }

        public virtual void Render()
        {
            foreach (KeyValuePair<string, Component> component in components)
            {
                if (component.Value.IsRenderable)
                {
                    component.Value.Render();
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using Dreamscape.TileEngine;
using Dreamscape.Entities;
using Dreamscape.Screens.Transitions;
using Dreamscape.Input;

namespace Dreamscape.Screens
{
    public class MapScreen : Gamescreen
    {
        private Map map;
        private Engine engine;

        public Engine Engine
        {
            get
            {
                return engine;
            }
        }
        //private NPC npc;
        //private FadeTransition transition;

        //public FadeTransition Transition
        //{
        //    get
        //    {
        //        return transition;
        //    }
        //}

        public Map CurrentMap
        {
            set
            {
                map = value;
                cam.AttachToMap(map);
                //Globals.Global.CurrentMap = map;
            }
            get
            {
                return map;
            }
        }

        public MapScreen(Engine engine) : base(engine.ScreenManager)
        {
            this.Updateable = true;
            this.Renderable = true;
            this.engine = engine;
            map = new Map(this);            
            //transition = new FadeTransition(1.0f);
        }

        public override void Load(string mapToLoad)
        {
            map.Load(mapToLoad);

            //Globals.Global.CurrentMap = map;
            engine.CurrentMap = map;

            Entity player = engine.EntityManager.RegisterEntity("player", new Entity());
            player.Load("Content/Actors/main_character.xml");


            //Entity npcEnt = Globals.Global.EntityManager.RegisterEntity("npc1", new NPC());
            //npcEnt.Load("Content/Actors/npc1.xml");
            //npc = Globals.Global.EntityManager.GetEntity<NPC>("npc1");
            //npc.Load("Content/Actors/npc1.xml");

            Entity camera = engine.EntityManager.RegisterEntity("default", new Camera(engine));
            cam = engine.EntityManager.GetEntity<Camera>("default");
            cam.AttachToMap(map);
            cam.SetTarget(player);

            base.Load();
        }

        public override void Unload()
        {
            base.Unload();
        }

        public override void Update(GameTime gameTime)
        {
            if (map.Transition.Transitioning)
            {
                map.Transition.Update(gameTime);
            }
            else
            {
                engine.EntityManager.Update(gameTime);
                map.Update(gameTime);
            }
            
            base.Update(gameTime);
        }

        public override void Render()
        {            
            map.Render();
            engine.EntityManager.Render();

            if (map.Transition.Transitioning)
            {
                map.Transition.Render();
            }

            base.Render();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Dreamscape.Input;

namespace Dreamscape.Screens
{
    public class DialogScreen : Gamescreen
    {
        private XmlDocument doc;
        private XmlNodeList nodelist;
        private Dialog nextDialog;
        private int index;

        Texture2D dialogBox;

        public DialogScreen(ScreenManager manager, string filename)
            : base(manager)
        {
            this.Updateable = true;
            this.Renderable = true;

            doc = new XmlDocument();
            doc.Load(filename);
            nodelist = doc.SelectNodes("Dialog/Block");
            index = 0;

            nextDialog = new Dialog(manager.Engine, nodelist[index]);
            dialogBox = screenManager.Engine.Content.Load<Texture2D>("Dialog/dialog_box.png");

            cam = screenManager.CurrentScreen.Camera;

            manager.InputManager.Push(new DialogInput(this));

            //dialogInput = new DialogInput(this);
        }

        public override void  Update(GameTime gameTime)
        {
 	        base.Update(gameTime);

            if (!nextDialog.DoneReading)
            {
                nextDialog.Update(gameTime);
            }
            else
            {
                if (index < nodelist.Count - 1)
                {
                    index++;
                    nextDialog = new Dialog(screenManager.Engine, nodelist[index]);
                }
                else
                {
                    //All the text blocks are done
                    nextDialog.DoneReading = true;
                    screenManager.PreviousScreen.Updateable = true;
                    screenManager.InputManager.Pop();
                    screenManager.PopScreen();
                }
            }
            //dialogInput.Update(gameTime);
        }

        public override void Render()
        {
            screenManager.Engine.SpriteBatch.Draw(dialogBox, new Rectangle((int)Math.Abs(cam.Transformation.Translation.X), (int)cam.Transformation.Translation.Y + (cam.CameraView.Height / 2), cam.CameraView.Width, cam.CameraView.Height / 2), new Rectangle(0, 0, 295, 28), Color.White);
            nextDialog.Render();
 	        base.Render();
            
        }

        public void NextDialog()
        {
            nextDialog.DoneReading = true;
            //index++;
            //nextDialog = new Dialog(screenManager.Engine, nodelist[index]);
        }
    }
}

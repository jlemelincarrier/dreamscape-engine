﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using Dreamscape.Entities;

namespace Dreamscape.Screens
{
    public class Gamescreen
    {
        protected ScreenManager screenManager;

        protected Camera cam;

        public Camera Camera
        {
            get
            {
                return cam;
            }
        }


        public bool Updateable
        {
            get;
            set;
        }

        public bool Renderable
        {
            get;
            set;
        }

        public Gamescreen(ScreenManager manager)
        {
            screenManager = manager;
            //screenManager.PushScreen(this);
        }

        public virtual void Load() { }
        public virtual void Load(string fileName) { }
        public virtual void Unload() { }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Render()
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Dreamscape.Entities;
using Dreamscape.Input;

namespace Dreamscape.Screens
{
    public class PauseScreen : Gamescreen
    {
        private PauseScreenInput input;

        public PauseScreen(ScreenManager manager)
            : base(manager)
        {
            input = new PauseScreenInput(manager.Engine);
            manager.PreviousScreen.Updateable = false;
            this.Updateable = true;
            this.Renderable = true;
            cam = manager.CurrentScreen.Camera;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            input.Update(gameTime);
        }

        public override void Render()
        {
            base.Render();

            screenManager.Engine.FontManager.RenderText("default", "Paused", new Vector2(Math.Abs(screenManager.CurrentScreen.Camera.Transformation.Translation.X), screenManager.CurrentScreen.Camera.Transformation.Translation.Y), Color.Black);
        }
    }
}

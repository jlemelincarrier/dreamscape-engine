﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Dreamscape.TileEngine;

namespace Dreamscape.Screens.Transitions
{
    public class FadeTransition : ScreenTransition
    {
        private Texture2D fadeTex;
        private Engine engine;

        public delegate void FadeIn(TeleportEventArgs args);
        public delegate void FadeOut();
        public event FadeIn FadeInCallback;
        //public event FadeOut FadeOutCallback;

        TeleportEventArgs teleportArgs;

        public FadeTransition(Engine engine, float duration)
            : base(duration)
        {
            FadingOut = true;
            FadingIn = false;
            TransitionAlpha = 0.0f;
            TransitionComplete = false;
            Transitioning = false;
            fadeTex = engine.Content.Load<Texture2D>("fadeTexture.png");
            this.engine = engine;
        }

        public override void Update(GameTime gameTime)
        {
            if (!TransitionComplete)
            {
                if (FadingOut)
                {
                    TransitionAlpha += gameTime.ElapsedGameTime.Milliseconds/1000.0f;

                    if (TransitionAlpha > Duration)
                    {
                        //FadingIn = true;
                        FadingOut = false;
                        TransitionComplete = true;
                        Transitioning = false;

                        if (FadeInCallback != null)
                        {
                            FadeInCallback(teleportArgs);
                        }
                    }
                }

                if (FadingIn)
                {
                    TransitionAlpha -= gameTime.ElapsedGameTime.Milliseconds/1000.0f;

                    if (TransitionAlpha < 0.0f)
                    {
                        TransitionAlpha = 0.0f;
                        FadingIn = false;
                        TransitionComplete = true;
                        Transitioning = false;
                    }
                }
            }

            //if (!FadingIn && !FadingOut)
            //{                
            //    TransitionComplete = true;
            //    Transitioning = false;
            //}

            engine.ScreenManager.CurrentScreen.Camera.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Render()
        {
            engine.SpriteBatch.Draw(fadeTex, 
                                    new Rectangle((int)Math.Abs(engine.ScreenManager.CurrentScreen.Camera.Transformation.Translation.X),
                                                  (int)engine.ScreenManager.CurrentScreen.Camera.Transformation.Translation.Y,
                                                  engine.Graphics.PreferredBackBufferWidth, 
                                                  engine.Graphics.PreferredBackBufferHeight), 
                                    new Color(Color.Black, TransitionAlpha));
            base.Render();
        }

        public void OnFadeIn(TeleportEventArgs args)
        {
            teleportArgs = args;
        }
    }
}

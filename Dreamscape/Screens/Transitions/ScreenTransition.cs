﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dreamscape.Screens.Transitions
{
    public class ScreenTransition
    {
        private float duration;
        private bool fadingIn, fadingOut;

        public bool Transitioning
        {
            get;
            set;
        }

        public bool TransitionComplete
        {
            get;
            set;
        }

        public bool FadingIn
        {
            set
            {
                fadingIn = value;
            }
            get
            {
                return fadingIn;
            }
        }

        public bool FadingOut
        {
            set
            {
                fadingOut = value;
            }
            get
            {
                return fadingOut;
            }
        }

        public float Duration
        {
            get
            {
                return duration;
            }
        }

        public float TransitionAlpha
        {
            get;
            set;
        }

        public ScreenTransition(float duration)
        {
            this.duration = duration;
        }

        public virtual void Update(GameTime gameTime) { }
        public virtual void Render() { }
    }
}

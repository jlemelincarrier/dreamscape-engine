﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;

namespace Dreamscape.Screens
{
    public class Dialog
    {
        private Engine engine;
        private bool doneReading;
        private string text;

        public bool DoneReading
        {
            set
            {
                doneReading = value;
            }
            get
            {
                return doneReading;
            }
        }

        public Dialog(Engine engine, XmlNode node)
        {
            this.engine = engine;
            text = node.InnerText;
            doneReading = false;
        }

        public void Update(GameTime gameTime)
        {
            //if (!doneReading)
            //{

            //}
        }

        public void Render()
        {
            //if (!doneReading)
            //{
                engine.FontManager.RenderText("default", text, Vector2.Zero, Color.White);
            //}
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Dreamscape.Screens
{
    public class ScreenManager
    {
        public Stack<Gamescreen> gamescreens;

        public ScreenManager()
        {
            gamescreens = new Stack<Gamescreen>();
        }

        public Gamescreen CurrentScreen
        {
            get
            {
                return gamescreens.Peek();
            }
        }

        public void RemoveScreen()
        {
            gamescreens.Pop();
        }

        public void AddScreen(Gamescreen screen)
        {
            gamescreens.Push(screen);
        }

        public void Update(GameTime gameTime)
        {
            foreach (Gamescreen screen in gamescreens)
            {
                if (screen.Updateable)
                {
                    screen.Update(gameTime);
                }
            }
        }

        public void Render()
        {
            foreach (Gamescreen screen in gamescreens)
            {
                if (screen.Renderable)
                {
                    screen.Render();
                }
            }
        }
    }
}

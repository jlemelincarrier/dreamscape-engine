﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Dreamscape.Screens;
using Dreamscape.Input;

namespace Dreamscape
{
    public class ScreenManager
    {
        private List<Gamescreen> gamescreens;
        private Gamescreen currentScreen;
        private Engine engine;

        private InputManager inputManager;

        public InputManager InputManager
        {
            get
            {
                return inputManager;
            }
        }

        public Engine Engine
        {
            get
            {
                return engine;
            }
        }

        public ScreenManager(Engine e)
        {
            gamescreens = new List<Gamescreen>();
            engine = e;
            inputManager = new InputManager();
        }

        public Gamescreen CurrentScreen
        {
            get
            {
                return currentScreen;
            }
        }

        public Gamescreen PreviousScreen
        {
            get
            {
                return gamescreens[gamescreens.IndexOf(currentScreen) - 1];
            }
        }

        public void PopScreen()
        {
            gamescreens.Remove(currentScreen);
            currentScreen = gamescreens[gamescreens.Count - 1];
        }

        public void PushScreen(Gamescreen screen)
        {
            currentScreen = screen;
            gamescreens.Add(screen);
        }

        public void Update(GameTime gameTime)
        {
            //foreach (Gamescreen screen in gamescreens)
            //{
            //    if (screen.Updateable)
            //    {
            //        screen.Update(gameTime);
            //    }
            //}

            for (int i = 0; i < gamescreens.Count; i++)
            {
                if (gamescreens[i].Updateable)
                {
                    gamescreens[i].Update(gameTime);
                }
            }

            inputManager.Update(gameTime);
        }

        public void Render()
        {
            //foreach (Gamescreen screen in gamescreens)
            //{
            //    if (screen.Renderable)
            //    {
            //        screen.Render();
            //    }
            //}
            for (int i = 0; i < gamescreens.Count; i++)
            {
                if (gamescreens[i].Renderable)
                {
                    gamescreens[i].Render();
                }
            }
        }
    }
}

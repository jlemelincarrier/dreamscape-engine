#include "EditorScene.h"

EditorScene::EditorScene(QObject *parent) : QGraphicsScene(parent){
    QPen pen;
    pen.setStyle(Qt::DotLine);

    for (int x=0; x<=(DEFAULT_MAP_WIDTH * DEFAULT_TILE_WIDTH); x+=DEFAULT_TILE_WIDTH)
        this->addLine(x,0,x,(DEFAULT_MAP_WIDTH * DEFAULT_TILE_WIDTH), pen);

    // Now add the horizontal lines, paint them green
    for (int y=0; y<=(DEFAULT_MAP_HEIGHT * DEFAULT_TILE_HEIGHT); y+=DEFAULT_TILE_HEIGHT)
        this->addLine(0,y,(DEFAULT_MAP_HEIGHT * DEFAULT_TILE_HEIGHT),y, pen);
}

void EditorScene::mousePressEvent(QGraphicsSceneMouseEvent *event){
    qDebug("scene was clicked");
    QPoint point(event->scenePos().x()/DEFAULT_TILE_WIDTH, event->scenePos().y()/DEFAULT_TILE_HEIGHT);
    qDebug() << point;

    //Need to add the half width to make sure the item is collided with completely.
    QGraphicsItem* ritem = this->itemAt((point.x()*DEFAULT_TILE_WIDTH)+(DEFAULT_TILE_WIDTH/2), (point.y()*DEFAULT_TILE_HEIGHT)+(DEFAULT_TILE_HEIGHT/2));
    if(ritem != 0){
        this->removeItem(ritem);
    }

    QGraphicsPixmapItem* item = new QGraphicsPixmapItem(*tilesheetData);
    item->setPos(point.x()*DEFAULT_TILE_WIDTH, point.y()*DEFAULT_TILE_HEIGHT);
    this->addItem(item);
}

void EditorScene::setTilesheetData(QPixmap data){
    tilesheetData = new QPixmap(data);
}

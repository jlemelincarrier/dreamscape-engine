#ifndef TILESHEETVIEW_H
#define TILESHEETVIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <QRect>
#include <QDebug>

#include "TilesheetScene.h"
#include "EditorScene.h"

class TilesheetView : public QGraphicsView{
public:
    TilesheetView(QWidget* parent, EditorScene* mapScene);
    void mouseReleaseEvent(QMouseEvent *event);
    TilesheetScene* getScene();
    void render(QPainter *painter, const QRectF &target, const QRect &source, Qt::AspectRatioMode aspectRatioMode);
private:
    QRectF* selectionRect;
    TilesheetScene* scene;
};

#endif // TILESHEETVIEW_H

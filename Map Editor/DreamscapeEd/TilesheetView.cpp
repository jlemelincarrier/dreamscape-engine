#include "TilesheetView.h"

TilesheetView::TilesheetView(QWidget *parent, EditorScene* mapScene) : QGraphicsView(parent){
    selectionRect = new QRectF(0, 0, 32, 32);
    scene = new TilesheetScene(parent, mapScene);
    setScene(scene);
}

void TilesheetView::mouseReleaseEvent(QMouseEvent *event){
    QPointF point = mapToScene(event->x()/32, event->y()/32);
    //qDebug() << "Tileview to Scene: " << point;
    selectionRect->moveTo(event->x(), event->y());
}

TilesheetScene* TilesheetView::getScene(){
    return scene;
}

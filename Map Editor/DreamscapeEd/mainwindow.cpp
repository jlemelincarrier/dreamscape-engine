#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    //setFixedSize(1200, 600);

    gridLayout = new QGridLayout(parent);

    mapView = new QGraphicsView(parent);
    editorScene = new EditorScene(mapView);

    tilesheetView = new TilesheetView(parent, editorScene);


    tilesheetView->setFixedWidth(400);

    QString fileName = QFileDialog::getOpenFileName(this, QObject::tr("Open File"),
                                                       "/home",
                                                       QObject::tr("Images (*.png *.xpm *.jpg)"));

    tilesheetView->getScene()->setTilesheet(new QPixmap(fileName));

    mapView->setScene(editorScene);

    toolbar = new QToolBar("toolbarItem", parent);

    QPixmap shapeIcon("icons/selection.png");

    toolbar->addAction(QIcon(shapeIcon), "Selection Mode");

    gridLayout->addWidget(toolbar, 0,0,1,2);

    gridLayout->addWidget(tilesheetView, 1, 0, 1, 1);
    gridLayout->addWidget(mapView, 1, 1, 1, 1);

    QWidget* window = new QWidget();

    window->setLayout(gridLayout);

    setCentralWidget(window);
}

MainWindow::~MainWindow()
{
    //delete ui;
}

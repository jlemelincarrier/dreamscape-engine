#-------------------------------------------------
#
# Project created by QtCreator 2013-07-23T13:17:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DreamscapeEd
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    EditorScene.cpp \
    TilesheetScene.cpp \
    TilesheetView.cpp

HEADERS  += mainwindow.h \
    EditorScene.h \
    TilesheetScene.h \
    TilesheetView.h

FORMS    += mainwindow.ui

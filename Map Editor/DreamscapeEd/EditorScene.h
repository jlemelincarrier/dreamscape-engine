#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneEvent>
#include <QPixmap>
#include <QPainter>
#include <QPoint>
#include <QDebug>
#include <QGraphicsPixmapItem>

#define DEFAULT_MAP_WIDTH   50
#define DEFAULT_MAP_HEIGHT  50
#define DEFAULT_TILE_WIDTH  32
#define DEFAULT_TILE_HEIGHT 32

class EditorScene : public QGraphicsScene{
public:
    EditorScene(QObject* parent = 0);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void setTilesheetData(QPixmap data);
private:
       QPixmap* tilesheetData;
};

#endif // EDITORSCENE_H

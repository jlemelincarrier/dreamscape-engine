#include "TilesheetScene.h"

TilesheetScene::TilesheetScene(QObject *parent, EditorScene* mapScene) : QGraphicsScene(parent){
    tilesheet = new QPixmap();
    //selectionRect = new QRectF(0, 0, 32,32);
    selectionRect = new QGraphicsRectItem(0, 0, 32, 32);
    selectionRect->setZValue(10);
    selectionRect->setBrush(QBrush(Qt::black, Qt::Dense4Pattern));
    QPen pen;
    pen.setStyle(Qt::DotLine);
    selectionRect->setPen(pen);
    addItem(selectionRect);

    this->mapScene = mapScene;
}

void TilesheetScene::setTilesheet(QPixmap *sheet){
    tilesheet = sheet;
    this->addPixmap(*tilesheet);
}

void TilesheetScene::mousePressEvent(QGraphicsSceneMouseEvent *event){
//    QPoint oldPoint;
//    if(selectionRect != NULL){
//        oldPoint = QPoint(selectionRect->left(), selectionRect->top());
//    }
    //qDebug() << "Old Rect Pos: " << oldPoint;
    //this->removeItem(this->itemAt(oldPoint.x(), oldPoint.y()));
    QPoint point((event->scenePos().x()/32), (event->scenePos().y()/32));
    selectionRect->setPos(point.x()*32, point.y()*32);
    qDebug() << point;
    //selectionRect->moveTo(point.x()*32, point.y()*32);
    //tilesheet->copy(selectionRect->x(), selectionRect->y(), selectionRect->boundingRect().width(), selectionRect->boundingRect().height());
    QPixmap data = tilesheet->copy(selectionRect->x(), selectionRect->y(), 32, 32);
    //this->addPixmap(data);
    this->mapScene->setTilesheetData(data);
    //this->addRect(*selectionRect, QPen(Qt::darkGreen), Qt::NoBrush);
}

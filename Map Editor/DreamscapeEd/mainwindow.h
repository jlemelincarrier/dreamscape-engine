#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsView>
#include <QGridLayout>
#include <QFileDialog>
#include <QPixmap>
#include <QToolBar>
#include <QIcon>

#include "EditorScene.h"
#include "TilesheetView.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    //Ui::MainWindow* ui;
    QGraphicsView* mapView;
    TilesheetView* tilesheetView;
    EditorScene* editorScene;
    QGridLayout* gridLayout;
    QToolBar* toolbar;
};

#endif // MAINWINDOW_H

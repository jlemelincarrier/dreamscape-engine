#ifndef TILESHEETSCENE_H
#define TILESHEETSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneEvent>
#include <QPixmap>
#include <QGraphicsRectItem>
#include <QDebug>
#include <QPen>
#include <QBrush>

#include "EditorScene.h"

class TilesheetScene : public QGraphicsScene{
public:
    TilesheetScene(QObject* parent = 0, EditorScene* mapScene = 0);
    void setTilesheet(QPixmap* sheet);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    QPixmap* tilesheet;
    //QRectF* selectionRect;
    QGraphicsRectItem* selectionRect;
    QPoint* selectionPoint;
    EditorScene* mapScene;
};

#endif // TILESHEETSCENE_H
